<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//app\Http\Controllers\Api\UserAuthController.php


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function () {

    Route::post('register', 'UserAuthController@registerUser');
    Route::post('login', 'UserAuthController@UserLogin');
    Route::post('getuserprofile', 'UserAuthController@getUserDetails');
    Route::post('updatauserdetails', 'UserAuthController@updateUserDetails');
    Route::post('deleteuserdetails', 'UserAuthController@deleteUserDetails');
    Route::get('getallprofiledetail', 'UserAuthController@getallUserProfiles');

});