<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;

class UserAuthController extends Controller
{
    public function __construct(){ 

      $this->usermodel = new User();

    }

    public function registerUser(Request $request)
    {

        try{   //*******validation  **********/
                $validator = Validator::make($request->all(), [
                    'name'          => 'required',
                    'mobile'        => 'required',
                    'address'       => 'required',
                    'state'         => 'required',
                    'country'       => 'required',
                    'email'         => 'required',
                    'password'      => 'required',
                ]);
                
                if ($validator->fails()) {
                    return response()->json(["status" => 401, "message" => $validator->errors()]);            
                }
                $userDetails = array(
                    'name'          => $request->name,
                    'mobile'        => $request->mobile,
                    'address'       => $request->address,
                    'state'         => $request->state,
                    'country'       => $request->country,
                    'email'         => $request->email,
                    'password'      =>  bcrypt($request->password),
                );

                //*******check email id exist**********/

                $getuserDetails = $this->usermodel->checkuser($request->email);

                //*******if not then**********//

                if(empty($getuserDetails->email)){

                    //*******store details in db**********//
                    $userDetails = User::create($userDetails);
                    $token = $userDetails->createToken('AuthApptoken')->accessToken;
                    
                    if($userDetails){
                        
                        return response()->json(["status" => 200, 'token' => $token,"message" => "user registered successfully.","data"=>$userDetails]); 

                    }else{

                        return response()->json(["status" => 401,"message" => "user not registered.","data"=> []]);

                    }

                }else{
                    return response()->json(["status" => 200, "message" => "email id taken.","data"=>[]]); 

                }
            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }
    public function UserLogin(Request $request)
    {
        try{   //*******validation  **********/
                $validator = Validator::make($request->all(), [
                    'email'       => 'required',
                    'password'    => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(["status" => 401, "message" => $validator->errors()]);            
                }
              //*******check user details using auth**********/
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
                    $getuserDetails = $this->usermodel->checkuser($request->email);
                    $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
                    return response()->json(["status" => 200, 'token' => $token,"message" => "user logged in successfully.","data"=> $getuserDetails]);

                } else {

                    return response()->json(["status" => 401,'error' => 'Unauthorised',"message" => "check given details."]);
                }

            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }
    public function getUserDetails(Request $request)
    {
        try{   //*******validation  **********/
                $validator = Validator::make($request->all(), [
                    'user_id'       => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(["status" => 401, "message" => $validator->errors()]);            
                }
               //*******get user details **********/
                $getuserDetails = $this->usermodel->getUserProfile($request->id);
           
                if($getuserDetails){ 

                    $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
                    return response()->json(["status" => 200, 'token' => $token,"message" => "user Details.",'data'=>$getuserDetails]);

                } else {

                    return response()->json(["status" => 200,"message" => "user details not found."]);
                }

            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }
    public function updateUserDetails(Request $request)
    {
        try{   //*******validation  **********/
                $validator = Validator::make($request->all(), [
                    'user_id'       => 'required',
                    'name'          => 'required',
                    'mobile'        => 'required',
                    'address'       => 'required',
                    'state'         => 'required',
                    'country'       => 'required',
                    'email'         => 'required',
                    'password'      => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(["status" => 401, "message" => $validator->errors()]);            
                }
                $userdata = array(
                    'name'          => $request->name,
                    'mobile'        => $request->mobile,
                    'address'       => $request->address,
                    'state'         => $request->state,
                    'country'       => $request->country,
                    'email'         => $request->email,
                    'password'      => $request->password,
                ) ;
                 //*******update user details **********/
                $getuserDetails = $this->usermodel->updateUserProfile($request->id,$userdata);
           
                if($getuserDetails){ 

                    $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
                    return response()->json(["status" => 200, 'token' => $token,"message" => "user Details updated.",'data'=>$getuserDetails]);

                } else {

                    return response()->json(["status" => 200,"message" => "user details not found."]);
                }

            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }
    public function deleteUserDetails(Request $request)
    {
        try{    //*******validation  **********/
                $validator = Validator::make($request->all(), [
                    'user_id'       => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json(["status" => 401, "message" => $validator->errors()]);            
                }
               //*******delete user details using user id **********/
                $getuserDetails = $this->usermodel->deleteUserProfile($request->user_id);

                if($getuserDetails){ 

                    return response()->json(["status" => 200, "message" => "user Details deleted.",'data'=>[]]);

                } else {

                    return response()->json(["status" => 200,"message" => "user details not found."]);
                }

            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }
    public function getallUserProfiles(Request $request)
    {
        try{   
               //*******get user details **********/
                $getuserDetails = $this->usermodel->getAllUserProfile();
                
                if($getuserDetails){ 

                    return response()->json(["status" => 200, "message" => "user Details.",'data'=>$getuserDetails]);

                } else {

                    return response()->json(["status" => 200,"message" => "user details not found."]);
                }

            }catch (\Exception $e){
                return response()->json(["status" => 400, "message" => $e->getMessage()]); 

            }
    }            
}

