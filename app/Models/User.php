<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable ,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'address',
        'state',
        'country',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //*************check user existance ****//

    public function checkuser($email){

        $data = User::where('email',$email)->first();
        return $data ;
    }
    //*************updateUserProfile ****//
    public function getUserProfile($user_id){

        $data = User::where('id',$user_id)->first();
        return $data ;
   
    }
    //*************updateUserProfile ****//
    public function updateUserProfile($user_id,$data){
        
        $data = User::where('id',$user_id)->update($data);
        return $data ;
       
    }
    //*************deleteUserProfile ****//
    public function deleteUserProfile($user_id){
        
        $data = User::where('id',$user_id)->delete();
        return $data ;
    }
    //***************getall user profile details using order by ****//
    public function getAllUserProfile(){
        
        $data = User::orderBy('created_at', 'desc')->get();;
        return $data ;
       
    }
}
